let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.webpackConfig({
    watchOptions: {
        ignored: ['node_modules', 'app', 'bootstrap', 'config', 'data', 'database', 'old', 'public_html', 'routes', 'stats', 'storage', 'tests', 'vendor']
    }
});

mix.setPublicPath('public_html/');

mix.disableNotifications();

mix.options({
    processCssUrls: false
})

mix.js('resources/assets/js/app.js', 'js/app.js').version();
mix.js('resources/assets/js/admin.js', 'js/admin.js').extract('tinymce').version();

mix.copy('node_modules/tinymce/skins', 'public_html/js/skins');

mix.sass('resources/assets/sass/app.scss', 'css/').version();
mix.sass('resources/assets/sass/backend-app.scss', 'css/').version();
