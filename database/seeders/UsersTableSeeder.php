<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Resets the users table
        User::truncate();

        // Generate 3 users/authors
        DB::table('users')->insert([
            [
                'name'  => 'gamescomevent redaction',
                'email'     => 'redaction@gamescomevent.com',
                'password'  => bcrypt('secret'),
            ],
            [
                'name'  => 'Jane Doe',
                'email'     => 'janedoe@test.com',
                'password'  => bcrypt('secret'),
            ],
            [
                'name'  => 'Jeffrey Sloof',
                'email'     => 'jeffrey_wii@hotmail.com',
                'password'  => bcrypt('secret'),
            ],
        ]);
    }
}
