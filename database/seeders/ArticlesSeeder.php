<?php

namespace Database\Seeders;

use App\Models\Article;
use Illuminate\Support\Facades\DB;

class ArticlesSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Resets the articles table
        Article::query()->delete();

        // Define attributes and their possible values
        $attributes = [
            'published_at' => [null, now(), now()->addYear()],
            'deleted_at' => [null, now(), now()->addYear()],
            'offline_at' => [null, now(), now()->addYear()],
            'image' => [null, 'example-image.jpg'],
        ];

        $combinations = $this->generateCombinations($attributes);

        // Inserts generic articles
        foreach ($combinations as $combination) {
            Article::factory()->state($combination)->create();
        }
    }
}
