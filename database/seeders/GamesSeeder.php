<?php

namespace Database\Seeders;

use App\Models\Game;
use App\Models\Platform;
use Illuminate\Support\Facades\DB;

class GamesSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Resets the games table
        Game::query()->delete();

        // Define attributes and their possible values
        $attributes = [
            'released_at' => [null, now()],
        ];

        $combinations = $this->generateCombinations($attributes);

        // Inserts generic games
        foreach ($combinations as $combination) {
            Game::factory()->state($combination)->hasPlatforms(1)->hasDevelopers(1)->hasPublishers(1)->create();
        }
    }
}
