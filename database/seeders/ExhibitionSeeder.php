<?php
namespace Database\Seeders;

use App\Models\Exhibition;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExhibitionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Resets the exhibition table
        Exhibition::query()->delete();

        Exhibition::factory()->create([
            'title'         => 'Gamescom 2025',
            'excerpt'       => 'This year marks the 17th edition of Gamescom. Since 2009, the Koelnmesse has opened its doors to gamers and fans to experience the creations of exhibitors like Microsoft Studios, Nintendo, and Sony. With over 335,000 visitors in 2024, it remains the largest gaming event in Europe.',
            'body'          => 'Gamescom is a convention where gamers feel completely at home. Every year, the exhibition halls of the Koelnmesse are filled with the latest (and retro!) games, consoles, and everything dedicated to gaming. Companies, both large and small, from the gaming industry showcase their newest creations and even offer visitors the chance to play some games before they hit the shelves.<br>
<br>
The event spans a total area of 230,000 m², divided across multiple halls. There are halls dedicated to the latest games and consoles, which form the largest section. Additionally, there’s a hall reserved for merchandise. Other areas are dedicated to hardware promotion, indie games, retro games, e-sports, and dining. Two separate halls, known as the "business area," are designated for trade visitors and the press. These areas require special tickets and open a day earlier for these audiences.<br>
<br>
Last year, Gamescom celebrated its 15th anniversary with over 335,000 attendees. Highlights included playable Xbox Series X and PlayStation 5 consoles, as well as events like League of Legends tournaments.<br>
<br>
This year, Gamescom takes place from August 20 to August 24, 2025, at the Koelnmesse in Cologne. The event kicks off with the Opening Night Live on August 19, featuring new announcements and world premieres. With more than 335,000 visitors and over 1,500 exhibitors expected, Gamescom 2025 promises to be a spectacular event for anyone passionate about the gaming industry.<br>
<br>
Whether you’re a dedicated gamer, a developer, or simply curious about the latest trends in gaming, Gamescom 2025 has something for everyone.',
            'address'       => 'Messepl. 1, 50679 Köln',
            'country'       => 'Germany',
            'latitude'      => '50.945271',
            'longitude'     => '6.979328',
            'slug'          => 'gamescom-2025',
            'starts_at'     => '2025-08-20 13:00:00',
            'ends_at'       => '2025-08-24 20:00:00',
        ]);
    }
}
