<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BaseSeeder extends Seeder
{
    /**
     * Generate all combinations of the given attributes.
     *
     * @param array $attributes
     * @return array
     */
    protected function generateCombinations(array $attributes)
    {
        $keys = array_keys($attributes);
        $values = array_values($attributes);

        $combinations = [[]];

        foreach ($values as $attributeValues) {
            $newCombinations = [];
            foreach ($combinations as $combination) {
                foreach ($attributeValues as $value) {
                    $newCombinations[] = array_merge($combination, [$value]);
                }
            }
            $combinations = $newCombinations;
        }

        // Map combinations back to their keys
        return array_map(function ($combination) use ($keys) {
            return array_combine($keys, $combination);
        }, $combinations);
    }
}
