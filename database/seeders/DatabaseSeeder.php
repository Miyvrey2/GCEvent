<?php

namespace Database\Seeders;

use App\Models\Game;
use Database\Seeders\ArticlesSeeder;
use Database\Seeders\ConsoleGameTableSeeder;
use Database\Seeders\PlatformsSeeder;
use Database\Seeders\ExhibitionSeeder;
use Database\Seeders\GamesSeeder;
use Illuminate\Database\Seeder;
use Database\Seeders\PagesTableSeeder;
use Database\Seeders\PublisherSeeder;
use Database\Seeders\RSSFeedsTableSeeder;
use Database\Seeders\RSSWebsitesSeeder;
use Database\Seeders\StandsTableSeeder;
use Database\Seeders\UsersTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $this->call([
//            UsersTableSeeder::class,
//            ]);
//        $this->call(PagesTableSeeder::class);
//        $this->call(PublisherSeeder::class);
        $this->call(PlatformsSeeder::class);
        $this->call(GamesSeeder::class);
//        $this->call(ConsoleGameTableSeeder::class);
//        $this->call(StandsTableSeeder::class);
//        $this->call(RSSFeedsTableSeeder::class);
        $this->call(ArticlesSeeder::class);
//        $this->call(RSSWebsitesSeeder::class);
        $this->call(ExhibitionSeeder::class);
    }
}
