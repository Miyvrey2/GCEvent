<?php

namespace Database\Seeders;

use App\Models\Platform;
use Illuminate\Support\Facades\DB;

class PlatformsSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'PlayStation 4',
            'Xbox one',
            'Switch',
        ];

        // Resets the platforms table
        Platform::query()->delete();

        // Define attributes and their possible values
        $attributes = [
            'title' => ['PlayStation 4', 'Xbox one', 'Switch'],
            'released_at' => [now()],
        ];

        $combinations = $this->generateCombinations($attributes);

        // Inserts generic platforms
        foreach ($combinations as $combination) {
            $combination['slug'] = str($combination['title'])->slug();
            Platform::factory()->state($combination)->create();
        }
    }
}
