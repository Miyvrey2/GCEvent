<?php

namespace Database\Factories;

use App\Models\Platform;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Console>
 */
class PlatformFactory extends Factory
{

    protected $model = Platform::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $title = str(fake()->sentence);

        return [
            'title' => str($title)->title(),
            'slug' => str($title)->slug(),
            'excerpt' => fake_paragraphs(1, true),
            'body' => fake_paragraphs(rand(3,6), true),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
