<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\Game;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Article>
 */
class ArticleFactory extends Factory
{

    protected $model = Article::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $title = str(fake()->sentence);

        return [
            'author_id' => User::factory(),
            'game_id' => Game::factory(),
            'title' => str($title)->title(),
            'slug' => str($title)->slug(),
            'excerpt' => fake_paragraphs(1, true),
            'body' => fake_paragraphs(rand(3,6), true),
            'created_at' => now(),
            'updated_at' => now(),
            'published_at' => now(),
        ];
    }
}
