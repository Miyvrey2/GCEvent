<?php

namespace Database\Factories;

use App\Models\Page;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Page>
 */
class PageFactory extends Factory
{

    protected $model = Page::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $title = str(fake()->sentence);

        return [
            'author_id' => User::factory(),
            'title' => str($title)->title(),
            'subtitle' => str(fake()->sentence),
            'slug' => str($title)->slug(),
            'excerpt' => fake_paragraphs(1, true),
            'body' => fake_paragraphs(rand(3,6), true),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
