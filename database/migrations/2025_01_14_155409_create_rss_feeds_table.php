<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rss_feeds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('site');
            $table->string('title');
            $table->text('url');
            $table->text('categories')->nullable();
            $table->unsignedInteger('game_id')->nullable()->index('rss_feeds_game_id_foreign');
            $table->dateTime('published_at');
            $table->dateTime('verified_at')->nullable();
            $table->softDeletes();
            $table->unsignedInteger('rss_website_id')->nullable()->index('rss_feeds_rss_website_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rss_feeds');
    }
};
