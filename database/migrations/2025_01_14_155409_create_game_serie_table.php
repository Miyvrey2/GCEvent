<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_serie', function (Blueprint $table) {
            $table->unsignedInteger('game_id')->nullable()->index('game_serie_game_id_foreign');
            $table->unsignedInteger('serie_id')->nullable()->index('game_serie_serie_id_foreign');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_serie');
    }
};
