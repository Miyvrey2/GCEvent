<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('game_publisher', function (Blueprint $table) {
            $table->foreign(['game_id'])->references(['id'])->on('games')->onUpdate('CASCADE')->onDelete('NO ACTION');
            $table->foreign(['publisher_id'])->references(['id'])->on('publishers')->onUpdate('CASCADE')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_publisher', function (Blueprint $table) {
            if (env('DB_CONNECTION') !== 'sqlite') {
                $table->dropForeign('game_publisher_game_id_foreign');
                $table->dropForeign('game_publisher_publisher_id_foreign');
            }
        });
    }
};
