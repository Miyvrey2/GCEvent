<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exhibition_game', function (Blueprint $table) {
            $table->foreign(['developer_id'])->references(['id'])->on('developers')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['exhibition_id'])->references(['id'])->on('exhibitions')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['game_id'])->references(['id'])->on('games')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['publisher_id'])->references(['id'])->on('publishers')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exhibition_game', function (Blueprint $table) {
            if (env('DB_CONNECTION') !== 'sqlite') {
                $table->dropForeign('exhibition_game_developer_id_foreign');
                $table->dropForeign('exhibition_game_exhibition_id_foreign');
                $table->dropForeign('exhibition_game_game_id_foreign');
                $table->dropForeign('exhibition_game_publisher_id_foreign');
            }
        });
    }
};
