<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('game_platform', function (Blueprint $table) {
            $table->foreign(['game_id'], 'console_game_game_id_foreign')->references(['id'])->on('games')->onUpdate('CASCADE')->onDelete('NO ACTION');
            $table->foreign(['platform_id'], 'console_game_platform_id_foreign')->references(['id'])->on('platforms')->onUpdate('CASCADE')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_platform', function (Blueprint $table) {
            if (env('DB_CONNECTION') !== 'sqlite') {
                $table->dropForeign('console_game_game_id_foreign');
                $table->dropForeign('console_game_platform_id_foreign');
            }
        });
    }
};
