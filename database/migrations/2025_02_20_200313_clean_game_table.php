<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('games', function (Blueprint $table) {
            if (env('DB_CONNECTION') !== 'sqlite') {
                $table->dropForeign(['publisher_id']);
                $table->dropForeign(['developer_id']);
                $table->dropForeign(['exhibitor_id']);
            }
            if (Schema::hasColumns('games', ['publisher_id', 'developer_id', 'exhibitor_id', 'line_up_year'])) {
                $table->dropColumn(['publisher_id', 'developer_id', 'exhibitor_id', 'line_up_year']);
            }
        });
    }

    public function down(): void
    {
        Schema::table('games', function (Blueprint $table) {
            $table->unsignedInteger('publisher_id')->nullable();
            $table->unsignedInteger('developer_id')->nullable();
            $table->unsignedInteger('exhibitor_id')->nullable();
            $table->string('line_up_year')->nullable();
        });
    }
};