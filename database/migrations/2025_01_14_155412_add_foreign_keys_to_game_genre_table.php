<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('game_genre', function (Blueprint $table) {
            $table->foreign(['game_id'])->references(['id'])->on('games')->onUpdate('CASCADE')->onDelete('NO ACTION');
            $table->foreign(['genre_id'])->references(['id'])->on('genres')->onUpdate('CASCADE')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_genre', function (Blueprint $table) {
            if (env('DB_CONNECTION') !== 'sqlite') {
                $table->dropForeign('game_genre_game_id_foreign');
                $table->dropForeign('game_genre_genre_id_foreign');
            }
        });
    }
};
