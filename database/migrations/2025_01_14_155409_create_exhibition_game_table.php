<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exhibition_game', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('developer_id')->nullable()->index('exhibition_game_developer_id_foreign');
            $table->unsignedInteger('exhibition_id')->nullable()->index('exhibition_game_exhibition_id_foreign');
            $table->unsignedInteger('game_id')->nullable()->index('exhibition_game_game_id_foreign');
            $table->unsignedInteger('publisher_id')->nullable()->index('exhibition_game_publisher_id_foreign');
            $table->string('hall')->nullable();
            $table->string('booth')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exhibition_game');
    }
};
