<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class OpenRouter
{
    protected string $apiKey;

    public function __construct()
    {
        $this->apiKey = config('services.openrouter.api_key');
    }

    public function ask(string $prompt): string
    {
        $response = Http::withHeaders([
            'Authorization' => "Bearer {$this->apiKey}",
        ])->post('https://openrouter.ai/api/v1/chat/completions', [
            'model'    => 'google/gemini-2.0-flash-lite-preview-02-05:free',
            'messages' => [['role' => 'user', 'content' => $prompt]],
        ]);

        return $response->json('choices.0.message.content', 'Geen antwoord.');
    }
}
