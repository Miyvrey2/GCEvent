<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pages';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'author_id',
        'title',
        'subtitle',
        'slug',
        'excerpt',
        'body',
        'sidebar_title',
        'sidebar_body',
        'image',
        'keywords',
        'published_at',
        'offline_at',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'published_at' => 'datetime',
        'offline_at' => 'datetime',
    ];

    public $enumStatuses = [
        'dr' => 'Draft',
        'sc' => 'Scheduled',
        'pu' => 'Published',
        'of' => 'Offline',
        'er' => 'Error',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    // Multiple pages belong to 1 user
    public function author()
    {
        return $this->belongsTo(User::class);
    }

    public function getDateAttribute()
    {
        return is_null($this->published_at) ? '' : $this->published_at->diffForHumans();
    }

    public function getBodyHtmlAttribute()
    {
        return $this->body;
    }

    public function getExcerptHtmlAttribute()
    {
        return e($this->excerpt);
    }

    public function getStatusAttribute()
    {
        // If the offline date has not been set, make a fake one for in the future so we can loop
        $this->offline_at = ($this->offline_at === null) ? Carbon::tomorrow() : $this->offline_at;

        if ($this->published_at == null) {
            $status = $this->enumStatuses['dr'];
        } else if ($this->published_at > Carbon::now()) {
            $status = $this->enumStatuses['sc'];
        } else if ($this->published_at < Carbon::now() && $this->offline_at > Carbon::now()) {
            $status = $this->enumStatuses['pu'];
        } else if ($this->offline_at < Carbon::now()) {
            $status = $this->enumStatuses['of'];
        } else {
            $status = $this->enumStatuses['er'];
        }

        return $status;
    }

    // Show only published Scope in where clause
    public function scopePublished($query)
    {
        return $query->where([
            ['published_at', '<=', Carbon::now()],
            ['offline_at', '>=', Carbon::now()]
        ])->orWhere([
            ['published_at', '<=', Carbon::now()],
            ['offline_at', '=', null]
        ]);
    }
}
