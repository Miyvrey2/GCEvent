<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExhibitionGame extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'exhibition_game';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'developer_id',
        'exhibition_id',
        'game_id',
        'publisher_id',
        'hall',
        'booth',
        'genre_id',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    // Multiple games belong to one exhibitor
    public function games()
    {
        return $this->hasMany(Game::class, 'id', 'game_id');
    }
    public function exhibitions()
    {
        return $this->hasMany(Exhibition::class, 'id', 'exhibition_id');
    }
    public function developers()
    {
        return $this->hasMany(Developer::class, 'id', 'developer_id');
    }
    public function publishers()
    {
        return $this->hasMany(Publisher::class, 'id', 'publisher_id');
    }

    public function game()
    {
        return $this->belongsTo(Game::class);
    }
    public function developer()
    {
        return $this->belongsTo(Developer::class);
    }
    public function publisher()
    {
        return $this->belongsTo(Publisher::class);
    }
    public function exhibition()
    {
        return $this->belongsTo(Exhibition::class);
    }
}
