<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exhibition extends Model
{
    use HasFactory;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'exhibitions';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'slug',
        'excerpt',
        'body',
        'latitude',
        'longitude',
        'starts_at',
        'ends_at',
        'address',
        'country',
        'aliases',
        'image',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'starts_at' => 'datetime',
        'ends_at' => 'datetime',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getBodyHtmlAttribute()
    {
        return $this->body;
    }

    // Show only upcoming Scope in where clause
    public function scopeUpcoming($query)
    {
        return $query->where('starts_at', '>=', Carbon::now());
    }

    // Multiple games belong to one exhibitor at gamescom
    public function booths()
    {
        return $this->hasMany(ExhibitionGame::class);
    }

    public function publishers()
    {
        return $this->belongsToMany(Publisher::class, 'exhibition_game',
            'exhibition_id', 'publisher_id');
    }
}
