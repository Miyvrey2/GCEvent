<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class LowercaseSegments
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $path = $request->path();

        // Controleer of de URL hoofdletters bevat
        if (preg_match('/[A-Z]/', $path)) {
            // Converteer naar kleine letters en redirect permanent
            $lowercasePath = strtolower($path);
            return redirect($lowercasePath, 301);
        }

        return $next($request);
    }
}