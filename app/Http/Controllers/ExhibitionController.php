<?php

namespace App\Http\Controllers;

use App\Models\Exhibition;

class ExhibitionController extends Controller
{
    /**
     * @param Exhibition $exhibition
     *
     * @return \Illuminate\View\View
     */
    public function show(Exhibition $exhibition)
    {
        return view('exhibition.show', compact('exhibition'));
    }

    /**
     * @param Exhibition $exhibition
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function lineup(Exhibition $exhibition)
    {
        return view('exhibition.lineup', compact('exhibition'));
    }

    /**
     * @param Exhibition $exhibition
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function exhibitors(Exhibition $exhibition)
    {
        return view('exhibition.publisher', compact('exhibition'));
    }
}
