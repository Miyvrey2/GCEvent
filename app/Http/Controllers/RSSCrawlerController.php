<?php

namespace App\Http\Controllers;

use App\Models\Platform;
use App\Models\Game;
use App\Models\RSSItem;
use App\Models\GamePlatform;
use App\Models\RSSWebsite;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class RSSCrawlerController extends Controller
{
    public $expire_in_days = 10;

    //
    public function crawl()
    {

        ini_set("default_charset", 'utf-8');
        ini_set('max_execution_time', 300); //300 seconds = 5 minutes


        // Setup for saving our crawlings
        $date_of_expire = Carbon::now()->subDays($this->expire_in_days);
        $platforms = Platform::all();

        $rss_websites = RSSWebsite::all();

        foreach($rss_websites as $site) {

            // Setup which site to crawl
            $url = $site->rss_url;
            $html = $this->getHTMLPage($url);
            $xml = simplexml_load_string($this->getHTMLPage($url));

            if($xml === false) {
                Log::error('Failed to load the XML from: ' . $url);
                continue;
            }

            // Process the crawled XML data to CSV
            foreach($xml->channel->item as $item) {

                $title 		= htmlspecialchars_decode($this->decodeXML($item->title->__toString(), $site->title));
                if($site->article_format == "guid") {
                    $url 	= htmlspecialchars(utf8_decode($item->guid->__toString()));
                } else {
                    $url 	= htmlspecialchars(utf8_decode($item->link->__toString()));
                }
                $datetime	= $item->pubDate->__toString();

                // reformat date
                $datetime = Carbon::createFromFormat($site->date_format, $datetime);
                if(isset($site->date_reformat) && $site->date_reformat == true) {
                    $datetime->addHours(2);
                }
                $datetime = $datetime->format("Y-m-d H:i:s");

                // Categories
                $categories = "";
                if(isset($item->category)) {
                    foreach($item->category as $category) {
                        $categories .= $category . ", ";
                    }
                }

                // Game title
                $game_id = $this->findGameidByNewsTitle($title);

                if($datetime > $date_of_expire) {

                    // Save the item to the CSV array
                    $rssItem = RSSItem::updateOrCreate(
                        [
                            'title' => $title,
                            'rss_website_id'  => $site->id,
                        ],
                        [
                            'site'  => $site->title,
                            'url'   => $url,
                            'published_at' => $datetime,
                            'categories' => $categories,
                        ]
                    );

                    if($rssItem->verified_at == null && $game_id != null) {
                        $rssItem->update(array('game_id' => $game_id));
                    }
                } else {

                    // If we reach the expire date, kill the foreach so we prevent long loaders
                    break;
                }
            }
        }

        $removeDuplicates = $this->removeDuplicates(false);
        $removeOldNews = $this->removeOldNews();

        return [
            'fetchRSSItems' => true,
            'saveRSSItems' => true,
            'removeDuplicates' => $removeDuplicates,
            'removeOldNews' => $removeOldNews,
        ];
    }

    public function removeDuplicates($view = true)
    {
        $rss_items = RSSItem::select(['title', 'rss_website_id', DB::raw(' COUNT(*) as `occurrences`')])
                             ->from('rss_feeds')
                             ->where('title', '!=', '')
                             ->groupBy('title', 'rss_website_id')
                             ->having('occurrences', '>', '1')
                             ->get();

        foreach($rss_items as $item) {

            Log::error('Removing duplicate: ' . $item->title);

            RSSItem::where([
                ['title', '=', $item->title],
            ])->first()->forceDelete();
        }

        if($view) {
            return "removed the duplicates";
        }
        return true;
    }

    private function removeOldNews() {

        $file = 'data/' .Carbon::now()->format("Y-m") . '.json';

        // Get all the old rss_items
        $db_items = RSSItem::where('published_at', '<', Carbon::now()->subDay($this->expire_in_days))->get();

        if(Storage::disk('local')->exists($file)) {
            // get the current rss_items in the json file
            $file_items = json_decode(Storage::disk('local')->get($file));

            // foreach item still in the DB
            foreach($db_items as $db_item) {

                $in_file = false;

                // And foreach item in the JSON file
                foreach($file_items as $file_item) {

                    if($file_item->title == $db_item->title) {
                        $in_file = true;
                    }
                }

                // If there was no occurrence, we place t he item in the JSON file
                if ($in_file == false) {

                    // Delete in DTB
                    $db_item->delete();

                    // Now put it into the file
                    unset($db_item['id']);
                    $file_items[] = $db_item;
                }
            }
        } else {

            $file_items = array();

            foreach($db_items as $db_item) {
                // Delete in DTB
                $db_item->delete();

                // Now put it into the file
                unset($db_item['id']);
                $file_items[] = $db_item;
            }
        }

        // Save the json_items
        Storage::disk('local')->put($file, json_encode($file_items));

        // Return if wanted
        return true;
    }

    /**
     * getHTMLPage($url) - Get the html from the requested page
     * @param $url
     *
     * @return bool|string
     */
    private function getHTMLPage($url)
    {
        return $this->getHTMLPageByCurl($url);
    }

    private function getHTMLPageByFileGetContents($url)
    {
        $opts = array(
            'http' => array (
                'method' => 'GET',
                'header' => "
                    User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36; \r\n
			        "
            ),
        );

        // Open the stream
        $context = stream_context_create($opts);

        // Get the page
        $page = file_get_contents($url, false, $context);

        // Return the value
        return $page;
    }

    private function getHTMLPageByCurl($url)
    {
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        $headers = array(
            "X-Custom-Header: value",
            "Content-Type: application/json",
            "User-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36"
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Get the page
        $page  = curl_exec($ch);

        // Get the HTTP code
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        // Return the value
        return $httpCode == 200 ? $page : '';
    }

    /**
     * decodeXML($string, $bool) - If bool is true, return the utf8_decoded string
     * @param $string
     * @param $bool
     *
     * @return string
     */
    private function decodeXML($string, $bool)
    {

        $string = str_replace('ï¿½','é', $string);

        if($bool === true) {
            return utf8_decode($string);
        }

        return $string;
    }

    private function findGameidByNewsTitle($newsTitle = '')
    {
        $games = Cache::remember('gameTitles', 60, function () {
            // Get a list with all the games, sorted on gameTitle length
            $rawTitles = Game::orderByRaw('CHAR_LENGTH(title) DESC')->pluck('id', 'title')->toArray();
            $gameTitles = [];
            foreach($rawTitles as $title => $id) {
                $gameTitles[$this->removeSpecialCharacters(strtolower($title))] = $id;
            }
            $rawAliases = Game::select(['id','aliases'])->whereNotNull('aliases')->where('aliases', '!=', '')->pluck('aliases', 'id')->toArray();
            $gameAliases = [];
            foreach($rawAliases as $key => $aliases) {
                $aliases = rtrim($aliases, ',');
                $aliases = explode(',', $aliases);
                foreach ($aliases as $alias) {
                    $gameAliases[$this->removeSpecialCharacters(strtolower($alias))] = $key;
                }
            }

            $names = array_merge($gameTitles, $gameAliases);
            $keys = array_map('strlen', array_keys($names));
            array_multisort($keys, SORT_DESC, $names);

            return $names;
        });


        // Remove some special characters
        $newsTitle = $this->removeSpecialCharacters(strtolower($newsTitle));

        foreach ($games as $title => $id) {
            $search = '/\b' . $title . '\b/i';
            // Check if the gameTitle is in the newsTitle
            if (preg_match($search, $newsTitle)) {
                return $id;
            }
        }

        return null;
    }

    public function suggestGameTitle()
    {
        $rss_items = RSSItem::where([
            ['published_at', '>=', Carbon::now()->subHours(48)],
            ['game_id', '=', null]
        ])
        ->orderBy('published_at', 'desc')
        ->get();

        // Get the keywords
        $file = 'data/RSSitems/keywords.json';
        if (Storage::disk('local')->exists($file)) {
            $file_items = json_decode(Storage::disk('local')->get($file), true);
        }

        $suggestions = [];

        // Loop trough the rss_items
        foreach($rss_items as $item){

            // get each word out of the title
            $title_as_array = explode(' ', $this->removeSpecialCharacters($item->title));

            // loop trough each word
            foreach ($title_as_array as $key => $word) {

                $word = strtolower($word);

                if ($word === "") {
                    continue;
                }

                if(array_key_exists($word, $file_items['items'])) {
                    if ($file_items['items'][ $word ]['in_game'] == 0 && $file_items['items'][ $word ]['other'] > 0) {
                        unset($title_as_array[ $key ]);
                    }
                }
            }

            if($title_as_array != [0 => ""]) {
                $suggestions[$item->id]["title"] = $item->title;
                $suggestions[$item->id]["words"] = $title_as_array;
            }
        }

        return view('backend.rssitem.suggest', compact('suggestions'));
    }

    private function removeSpecialCharacters($string)
    {
        $string = preg_replace("/&#?[a-z0-9]{2,8};/i","",$string);
        $string = str_replace(':', '', $string);
        $string = str_replace(';', '', $string);
        $string = str_replace('[', '', $string);
        $string = str_replace(']', '', $string);
        $string = str_replace('-', '', $string);
        $string = str_replace('!', '', $string);
        $string = str_replace('?', '', $string);
        $string = str_replace('/', '', $string);
        $string = str_replace('"', '', $string);
        $string = str_replace('\'', '', $string);
        $string = str_replace('\`', '', $string);
        $string = str_replace('  ', ' ', $string);
        $string = str_replace('   ', ' ', $string);

        return $string;
    }
}
