<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\StoreOrUpdateGame;
use App\Models\Platform;
use App\Models\Developer;
use App\Models\Genre;
use App\Http\Controllers\Controller;
use App\Models\Game;
use App\Models\Publisher;
use App\Models\Release;
use App\Models\RSSItem;
use App\Models\Serie;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class GameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $games = Game::all();


        return view('backend.game.index', compact('games'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($title = null)
    {

        // Get the developers, games, publishers, platforms and genres
        $developers = Developer::orderBy('title')->get();
        $publishers = Publisher::orderBy('title')->get();
        $platforms = $platformsReleasedOn = Platform::orderBy('released_at')->get();
        $genres = Genre::orderBy('title')->get();
        $series = Serie::orderBy('title')->get();
        $games = Game::all(); // Doesn't have to be ordered since it is for auto-completion

        // Initiate a new game with some defined values
        $game = new Game();
        $game->released_at = date("Y") . "-00-00";
        $game->aliases = null;
        if($title != null){
            $game->title = $title;
        }

        return view('backend.game.create', compact('developers', 'games','publishers', 'game', 'platforms', 'platformsReleasedOn', 'genres', 'series'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrUpdateGame $request)
    {
        // Get the validated data from request validator
        $data = $request->validated();

        // Make slug readable
        $data['slug'] = $this->slugify($data['slug']);

        // Make from multiple keywords 1
        $data['aliases'] = @$this->implodeOrNull($data['aliases']);

        // Save into another databse
        //        DB::purge('mysql');
        //        Config::set('database.connections.mysql.database', 'db_test');

        // Save
        $game = Game::create($data);

        // Sync the game and it's platforms
        $game->platforms()->sync($request['platforms']);

        // Sync the game and it's genres
        $game->genres()->sync($request['genres']);

        // Save the releases
        $releases = $request->get('releases');
        $this->saveReleases($game, $releases);

        // Sync the game and it's publishers
        if($request['publishers'] != null) {
            if ($this->is_array_value_nummeric($request['publishers'])) {
                $game->publishers()->sync($request['publishers']);
            }
            else {

                $publisher_array = [];

                foreach ($request['publishers'] as $newpublisher) {
                    if ( ! is_numeric($newpublisher)) {
                        $id = Publisher::create(['title' => $newpublisher,
                            'slug' => $this->slugify($newpublisher)
                        ])->id;

                        $publisher_array[] = $id;
                    }
                    else {
                        $publisher_array[] = $newpublisher;
                    }
                }

                $game->publishers()->sync($publisher_array);
            }
        }

        // Sync the game and it's developers
        if($request['developers'] != null) {
            if ($this->is_array_value_nummeric($request['developers'])) {
                $game->developers()->sync($request['developers']);
            }
            else {

                $developer_array = [];

                foreach ($request['developers'] as $newdeveloper) {
                    if ( ! is_numeric($newdeveloper)) {
                        $id = Developer::create(['title' => $newdeveloper,
                            'slug' => $this->slugify($newdeveloper)
                        ])->id;

                        $developer_array[] = $id;
                    }
                    else {
                        $developer_array[] = $newdeveloper;
                    }
                }

                $game->developers()->sync($developer_array);
            }
        }

        // Sync the game and it's publishers
        $game->series()->sync($request['series']);

        return redirect('/admin/games');
    }

    /**
     * Display the specified resource.
     *
     * @param Game $game
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {

        $game->rssFeeds = RSSItem::where('game_id', '=', $game['id'])->get();

        return view('game.show', compact('game'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Game  $game
     * @return \Illuminate\Http\Response
     */
    public function edit(Game $game)
    {
        // Get the developers, games, publishers, platforms and genres
        $developers = Developer::orderBy('title')->get();
        $publishers = Publisher::orderBy('title')->get();
        $platformsReleasedOn = Platform::orderBy('released_at')->get();
        $platforms = Platform::orderBy('released_at')->get();
        $genres = Genre::orderBy('title')->get();
        $series = Serie::orderBy('title')->get();
        $games = Game::all(); // Doesn't have to be ordered since it is for auto-completion
        $regions = Release::select('region')->orderBy('region')->get();

        if($game['aliases'] != "") {
            $game['aliases'] = explode(',', $game['aliases']);
        } else {
            $game->keywords = null;
        }

        // Get the platforms and genres that are not already listed
        $platforms = $this->unset_arrayitem_from_array_all_if_already_used($game->platforms, $platforms);
        $genres = $this->unset_arrayitem_from_array_all_if_already_used($game->genres, $genres);
        $publishers = $this->unset_arrayitem_from_array_all_if_already_used($game->publishers, $publishers);
        $developers = $this->unset_arrayitem_from_array_all_if_already_used($game->developers, $developers);
        $series = $this->unset_arrayitem_from_array_all_if_already_used($game->series, $series);

        return view('backend.game.edit', compact('developers', 'games','publishers', 'game', 'platforms', 'platformsReleasedOn', 'genres', 'series', 'regions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  StoreOrUpdateGame $request
     * @param  Game  $game
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreOrUpdateGame $request, Game $game)
    {

        // Make from multiple keywords 1
        $request['aliases'] = @$this->implodeOrNull($request['aliases']);

        // Save the updates
        $game->update($request->except('releases'));

        // Sync the game and it's platforms
        $game->platforms()->sync($request['platforms']);

        // Sync the game and it's genres
        $game->genres()->sync($request['genres']);

        $releases = $request->get('releases');
        $this->saveReleases($game, $releases);

        // Sync the game and it's publishers
        if($request['publishers'] != null) {
            if ($this->is_array_value_nummeric($request['publishers'])) {
                $game->publishers()->sync($request['publishers']);
            }
            else {

                $publisher_array = [];

                foreach ($request['publishers'] as $newpublisher) {
                    if ( ! is_numeric($newpublisher)) {
                        $id = Publisher::create(['title' => $newpublisher,
                            'slug' => $this->slugify($newpublisher)
                        ])->id;

                        $publisher_array[] = $id;
                    }
                    else {
                        $publisher_array[] = $newpublisher;
                    }
                }

                $game->publishers()->sync($publisher_array);
            }
        }

        // Sync the game and it's developers
        if($request['developers'] != null) {
            if( $this->is_array_value_nummeric($request['developers']) ) {
                $game->developers()->sync($request['developers']);
            } else {

                $developer_array = [];

                foreach($request['developers'] as $newdeveloper) {
                    if(!is_numeric($newdeveloper)) {
                        $id = Developer::create(['title' => $newdeveloper, 'slug' => $this->slugify($newdeveloper)])->id;

                        $developer_array[] = $id;
                    } else {
                        $developer_array[] = $newdeveloper;
                    }
                }

                $game->developers()->sync($developer_array);
            }
        }

        // Sync the game and it's series
        $game->series()->sync($request['series']);

        if($request['backToEnrich'] == 'true') {
            return Redirect::to('/admin/games/enrich');
        }

        return Redirect::to('/admin/games');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Game  $game
     * @return \Illuminate\Http\Response
     */
    public function destroy(Game $game)
    {
        // Delete the game
        $game->delete();

        return Redirect::to('/admin/games');
    }

    public function findPublisher(Game $game)
    {
        $detailedDescription = $this->fetchFromGoogle($game->title);
        $suggestPublishers = $this->findDeveloperOrPublisherFromString($detailedDescription, 'published by');

        $suggestPublishers = array_combine($suggestPublishers, $suggestPublishers);

        $suggestPublishers = Publisher::whereIn('title', $suggestPublishers)->get()->pluck('title', 'id')->toArray() ?: $suggestPublishers;

        return $suggestPublishers;
    }

    public function findDeveloper(Game $game)
    {
        $detailedDescription = $this->fetchFromGoogle($game->title);
        $suggestDevelopers = $this->findDeveloperOrPublisherFromString($detailedDescription);

        $suggestDevelopers = array_combine($suggestDevelopers, $suggestDevelopers);

        $suggestDevelopers = Developer::whereIn('title', $suggestDevelopers)->get()->pluck('title', 'id')->toArray() ?: $suggestDevelopers;

        return $suggestDevelopers;
    }

    public function recentlyInRSS()
    {
        $games = Game::with(['RSSFeeds' => function($query) {
                         $query->where([['published_at', '>=', Carbon::now()->subHours(48)], ['game_id', '!=', null]]);
                     }])
                     ->get()
                     ->sortByDesc(function($games) {
                        return $games->RSSFeeds->count();
                     });

        return view('backend.game.recently', compact('games'));
    }

    public function recentlyInRSSCoupling()
    {
        $games = Game::with(['RSSFeeds' => function($query) {
            $query->where([['published_at', '>=', Carbon::now()->subHours(48)], ['game_id', '!=', null]]);
        }])
                     ->get()
                     ->sortByDesc(function($games) {
                         return $games->RSSFeeds->count();
                     });

        $rss_feeds_titles = [];

        foreach($games as $game) {

            // remove the gametitle or gamealias from the feedtitle
            if(count($game->RSSFeeds) > 1) {
                foreach($game->RSSFeeds as $feed) {
                    $feedTitle = $feed->title;
                    $feedTitle = str_replace($game->title, '', $feedTitle);

                    foreach(explode(',', $game->aliases) as $alias) {
                        $feedTitle = str_replace($alias, '', $feedTitle);
                    }

                    // Clean feed title
                    $feedTitle = $this->clean($feedTitle);

                    $rss_feeds_titles[$game->id][$feed->id] = $feedTitle;
                }
            }

        }

        $article_word_suggestion = [];

        // Loop trough the rss titles
        foreach ($rss_feeds_titles as $key => $game) {

            $article_word_suggestion[$key] = [];

            foreach($game as $feed_title) {

                // get each word out of the title
                $title_as_array = explode(' ', $feed_title);

                // loop trough each word and count
                foreach ($title_as_array as $word) {
                    if($word === "") {
                        continue;
                    }

                    $word = strtolower($word);
                    if (!array_key_exists($word, $article_word_suggestion[$key])) {
                        $article_word_suggestion[$key][$word] = 1;
                    } else {
                        $article_word_suggestion[$key][$word] += 1;
                    }
                }
            }

//            $article_word_suggestion[$key] = $this->unset_if_one($article_word_suggestion[$key]);

            // Sort on value amount
            arsort($article_word_suggestion[$key]);
        }

        dd($article_word_suggestion);

        return view('backend.game.recently', compact('games'));
    }

    public function enrich()
    {
        $suggestDevelopers = [];
        $suggestPublishers = [];
        $developers = Developer::orderBy('title')->get()->pluck('title', 'id')->toArray();
        $publishers = Publisher::orderBy('title')->get()->pluck('title', 'id')->toArray();

        // Get a random game that is not enriched yet
        $game = Game::where([
            ['created_at', '>=', Carbon::now()->subHours(24 * 7)]
        ])->doesntHave('publishers')->doesntHave('developers')->inRandomOrder()->first();

        $amountLeft = Game::where([
            ['created_at', '>=', Carbon::now()->subHours(24 * 7)]
        ])->doesntHave('publishers')->doesntHave('developers')->inRandomOrder()->count();

        if($game == null) {
            return view('backend.game.enrich', compact('game'));
        }

        // Retrieve enriched data from Google
        $googleData = $this->fetchFromGoogle($game->title);
        if($googleData == false) {
            $googleData = $this->fetchFromGoogle($game->title . " game");
        }
        if($googleData) {
            $suggestDevelopers = $this->findDeveloperOrPublisherFromString($googleData);
            $suggestPublishers = $this->findDeveloperOrPublisherFromString($googleData, 'published by');

            $suggestDevelopers = array_combine($suggestDevelopers, $suggestDevelopers);
            $suggestPublishers = array_combine($suggestPublishers, $suggestPublishers);

            $suggestDevelopers = Developer::whereIn('title', $suggestDevelopers)->get()->pluck('title', 'id')->toArray() ?: $suggestDevelopers;
            $suggestPublishers = Publisher::whereIn('title', $suggestPublishers)->get()->pluck('title', 'id')->toArray() ?: $suggestPublishers;
        }

        if($googleData == false) {
            $url = 'https://www.google.com/search?igu=1&' . http_build_query(['q' => strtolower($game->title)]);
            $googleData = '<a href="' . $url . '" target="_blank">' . $url . '</a>';
        }

        return view('backend.game.enrich', compact('game', 'googleData', 'developers', 'publishers', 'suggestDevelopers', 'suggestPublishers', 'amountLeft'));
    }

    public function ajax()
    {
        $game = Game::with(['publishers', 'developers'])->where('id', '=', request('game_id'))->first()->makeHidden(['id', 'created_at', 'updated_at', 'exhibitor_id', 'line_up_year', 'publisher_id', 'developer_id']);
        $game->released_at = null;
        $game->series = null;
        $game->platforms = null;
        $game->genres = null;

        $json = $game->toJSON();

        $response = \App\Facades\OpenRouterFacade::ask("
        Gebruik de data die wordt aangeleverd en verrijk het object in het Engels. 
        Geef voor elk van deze attributen de juiste waardes en return enkel als json array. 
        Indien een attribuut leeg is, zoek deze waarde. Indien geen resultaten toon hiervoor de waarde 'null'.
        Indien een attribuut enkel [] is, zoek deze waarde. Indien geen resultaten toon hiervoor de waarde 'null'.
        released_at moeten in het formaat 'YYYY-MM-DD' zijn. 
        Als er meerdere waardes zijn, geef deze dan ; gescheiden terug.
        Indien een lege body of excerpt, genereer voor elk een waarde die los staan van elkaar. 
        De data is $json");

        $response = str_replace(['```', 'json', "\n", '{', '}', '[', ']', '"'], '', $response);
        $response = explode(',  ', $response);
        $data = [];

        foreach($response as $line) {
            $item = explode(':', $line);
            $key = trim($item[0]);
            $value = isset($item[1]) ? trim($item[1]) : '';
            $data[$key] = str_replace(';', "\n", $value);
        }

        $response = json_encode($data);

        return response()->json($data);
    }

    /**
     * Remove arrayitem from the "all"-array out of the database if the arrayitem is already selected for this game
     *
     * @param $already_in_use_array
     * @param $full_array
     *
     * @return mixed
     */
    private function unset_arrayitem_from_array_all_if_already_used($already_in_use_array, $full_array) {
        foreach($already_in_use_array as $already_in_use_array_item) {
            foreach($full_array as $key => $value) {
                if($already_in_use_array_item->id == $value->id) {
                    $full_array->forget($key);
                }
            }
        }

        return $full_array;
    }

    private function clean($string) {
        return preg_replace('/[^A-Za-z0-9\- ]/', '', $string); // Removes special chars.
    }

    private function unset_if_one($array) {

        foreach($array as $key => $value){

            if ($value === 1) {
                unset($array[$key]);
            }
        }

        return $array;
    }

    private function googleKnowledgeSearch($term)
    {
        // https://developers.google.com/knowledge-graph/#knowledge_graph_entities
        // https://kgsearch.googleapis.com/v1/entities:search?query=death+stranding&key=env('GOOGLE_KNOWLEDGE_SEARCH_KEY')&limit=1

        $service_url = 'https://kgsearch.googleapis.com/v1/entities:search';
        $params = array(
            'query' => $term,
            'limit' => 1,
            'key' => env('GOOGLE_KNOWLEDGE_SEARCH_KEY'));
        $url = $service_url . '?' . http_build_query($params);

        $opts = array(
            'http' => array (
                'method' => 'GET',
                'header' => "
                    User-Agent: Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36; \r\n
			        "
            ),
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            ),
        );

        // Open the stream
        $context = stream_context_create($opts);

        // Return the page
        return file_get_contents($url, false, $context);
    }

    private function is_array_value_nummeric($array)
    {
        foreach($array as $key => $value) {
            if (!is_numeric($value)) {
                return false;
            }
        }

        return true;
    }

    private function curl_page($url)
    {
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        $headers = array(
            "X-Custom-Header: value",
            "Content-Type: application/json",
            "User-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36"
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Get the page
        $page  = curl_exec($ch);

        curl_close($ch);

        // Return the value
        return $page;
    }

    private function saveReleases($game, $releases)
    {
        Release::where('game_id', '=', $game->id)->delete();

        if(isset($releases) &&count($releases) > 0) {
            foreach($releases as $release) {
                if(isset($release['released_at']) && $release['released_at'] != null) {
                    foreach($release['console'] as $console) {
                        Release::create([
                            'game_id'       => $game->id,
                            'released_at'   => $release['released_at'],
                            'console'       => $console,
                            'region'        => $release['region'],
                            'edition'       => $release['edition']
                        ]);
                    }
                }
            }
        }

    }


    private function fetchFromGoogle($query)
    {
        // Get from gogle the knowledge search
        $json = $this->googleKnowledgeSearch($query);
        $json_decode = json_decode($json);

        // If the json is empty, return false
        if(!$json_decode->itemListElement) {
            return false;
        }

        // Get the title and description
        $title = $json_decode->itemListElement[0]->result->name;
        if(!isset($json_decode->itemListElement[0]->result->detailedDescription)) {
            return false;
        }
        $detailedDescription = $json_decode->itemListElement[0]->result->detailedDescription->articleBody;

        // If the title is not the same, return false
        if($query != $title) {
            return false;
        }

        return $detailedDescription;
    }

    private function findDeveloperOrPublisherFromString($string, $find = 'developed by') {
        if($find == 'developed by') {
            $exclude = 'published by';
        } else {
            $find = 'published by';
            $exclude = 'developed by';
        }
        $start = "";
        $end = "";
        $explode = false;

        if (strpos($string, $find) !== false) {
            $start = $find;
        }
        if ($end == '' && strpos($string, 'and '. $exclude) !== false) {
            $end = 'and ' . $exclude;
        } elseif ($end == '' && strpos($string, 'and ' . $find) !== false) {
            $explode = 'and ' . $find;
            $end = '.';
        } elseif ($end == '' && strpos($string, 'and') !== false) {
            $explode = 'and';
            $end = '.';
        } else {
            $end = '.';
        }
        // Return all found company names
        $names[] = get_string_between($string, $start, $end);
        if($explode) {
            $names = explode($explode, $names[0]);
        }
        if (strpos($names[0], 'and') !== false) {
            $names = explode('and', $names[0]);
        }
        $names = array_map('trim', $names);

        return $names;
    }
}
