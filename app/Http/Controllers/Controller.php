<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function slugify($slug)
    {

        // Search and replace edge cases in the slug so it becomes URL proof
        $slug = str_replace(" ", "-", $slug);
        $slug = strtolower($slug);
        $slug = preg_replace("/[^a-zA-Z0-9-]+/", "", $slug);

        return $slug;
    }

    public function implodeOrEmptyString($keywords)
    {
        if(isset($keywords)) {
            $keywords = implode(",", $keywords);
        }

        return $keywords ?? null;
    }

    public function implodeOrNull($keywords)
    {
        if(isset($keywords)) {
            $keywords = implode(",", $keywords);
        }

        return $keywords ?? null;
    }
}
