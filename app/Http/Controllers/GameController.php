<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\Publisher;
use App\Models\RSSItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $games = Game::orderBy('title', 'asc')->get();

        foreach ($games as $key => $game) {
            if ($game['title'] === '---') {
                unset($games[$key]);
            }
        }

        return view('game.index', compact('games'));
    }

    // Show all listed games for 2022
    public function listed()
    {
        // current year
        $currentYear = date('Y');

        $games = Game::orderBy('title', 'asc')
                     ->where([
                         ['released_at', '>=', $currentYear.'-01-01'],
                         ['released_at', '<=', $currentYear.'-12-31'],
                     ])
            ->orderBy('title', 'ASC')->get();

        return view('game.listed', compact('games'));
    }

    // Show upcoming games for the next month
    public function upcoming()
    {
        $games = Game::orderBy('title', 'asc')
                     ->where([
                         ['released_at', '<=', Carbon::now()->addMonth()],
                         ['released_at', '>=', Carbon::now()->subDay()],
                     ])
                     ->get();

        return view('game.upcoming', compact('games'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Game  $game
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {
        $game->rssFeeds = RSSItem::where('game_id', '=', $game->id)->get();

        return view('game.show', compact('game'));
    }
}
