<?php
namespace App\Http\Repositories;

use App\Models\Article;
use Illuminate\Support\Collection;

class ArticleRepository
{
    /**
     * Get 5 latest recent articles
     *
     * @return Collection
     */
    public function recentArticles()
    {
        $recent_articles = Article::published()->orderBy('published_at', 'DESC')->limit(5)->get();

        return $recent_articles;
    }
}
