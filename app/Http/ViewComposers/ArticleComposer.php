<?php

namespace App\Http\ViewComposers;

use App\Models\Article;
use Illuminate\View\View;

class ArticleComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('recent_articles', Article::recentArticles());
    }
}
