<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SlugToLowerCase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slug:to-lower-case';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets all slugs to lowercase';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $entities = [
            'games' => \App\Models\Game::class,
            'platforms' => \App\Models\Platform::class,
            'series' => \App\Models\Serie::class,
            'publishers' => \App\Models\Publisher::class,
            'developers' => \App\Models\Developer::class,
            'articles' => \App\Models\Article::class,
            'pages' => \App\Models\Page::class,
        ];

        $this->info('Setting all slugs to lowercase...');

        foreach($entities as $entityName => $entity) {
            $counter = 0;
            $entities = $entity::all();
            foreach ($entities as $entity) {
                if($entity->slug !== strtolower($entity->slug)) {
                    $entity->slug = strtolower($entity->slug);
                    $entity->save();
                    $counter++;
                }
            }
            $this->info($counter . ' ' . $entityName . ' slugs updated to lowercase...');
        }

        $this->info('Done.');


        return Command::SUCCESS;
    }
}
