<?php

namespace App\Console\Commands;

use App\Models\Exhibition;
use App\Models\ExhibitionGame;
use App\Models\Game;
use Illuminate\Console\Command;

class migrateGameRelationsToPivots extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gce:migrate-game-relations-to-pivots';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $games = Game::all();

        foreach($games as $game) {
            if($game->exhibitor_id !== null) {
                $year = $game->line_up_year ?? '2018';
                $exhibition = Exhibition::where('title', 'like', "%$year")->first();

                if($exhibition === null) {
                    $this->line("Creating exhibition for year $year");
                    die();
                }

                $exhibitionGame = ExhibitionGame::create([
                    'game_id' => $game->id,
                    'exhibition_id' => $exhibition->id,
                    'publisher_id' => $game->exhibitor_id,
                ]);
            }

            $game->publishers()->syncWithoutDetaching($game->publisher_id);
            $game->developers()->syncWithoutDetaching($game->developer_id);

            $game->publisher_id = null;
            $game->developer_id = null;
            $game->exhibitor_id = null;
            $game->line_up_year = null;
            $game->save();
        }

        return Command::SUCCESS;
    }
}
