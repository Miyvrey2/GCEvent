<?php

function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

function fake_paragraphs($count = 1, $asText = false){
    $paragraphs = [];

    for ($i = 0; $i < $count; $i++) {
        $paragraphs[] = '<p>' . fake()->paragraphs(rand(1,3), true) . '</p><br>';
    }

    return $asText ? implode("\n", $paragraphs) : $paragraphs;
}
