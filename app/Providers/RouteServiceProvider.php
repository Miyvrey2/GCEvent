<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\Developer;
use App\Models\Game;
use App\Models\Genre;
use App\Models\Page;
use App\Models\Platform;
use App\Models\Publisher;
use App\Models\RSSItem;
use App\Models\RSSWebsite;
use App\Models\Serie;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * Typically, users are redirected here after authentication.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, and other route configuration.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Route::bind('article', function($slug) {
            return Article::where('slug', $slug)->first();
        });

        Route::bind('developer', function($slug) {
            return Developer::where('slug', $slug)->first();
        });

        Route::bind('game', function($slug) {
            return Game::where('slug', $slug)->first();
        });

        Route::bind('genre', function($slug) {
            return Genre::where('slug', $slug)->first();
        });

        Route::bind('page', function($slug) {
            return Page::published()->where('slug', $slug)->first();
        });

        Route::bind('platform', function($slug) {
            return Platform::where('slug', $slug)->first();
        });

        Route::bind('publisher', function($slug) {
            return Publisher::where('slug', $slug)->first();
        });

        Route::bind('rssitem', function($id) {
            return RSSItem::where('id', $id)->first();
        });

        Route::bind('rsswebsite', function($id) {
            return RSSWebsite::where('id', $id)->first();
        });

        Route::bind('serie', function($slug) {
            return Serie::where('slug', $slug)->first();
        });

        // Admin
        Route::bind('admin_pages', function($id) {
            return Page::where('id', $id)->first();
        });

        $this->configureRateLimiting();

        $this->routes(function () {
            Route::middleware('api')
                ->prefix('api')
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->group(base_path('routes/web.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });
    }
}
