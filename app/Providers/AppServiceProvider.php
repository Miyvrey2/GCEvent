<?php

namespace App\Providers;

use App\Services\OpenRouter;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(OpenRouter::class, function ($app) {
            return new OpenRouter();
        });
        $this->app->singleton('openrouter', function ($app) {
            return new OpenRouter();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);
    }
}
