<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\Auth\ActivateController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\ArticleController as BackendArticleController;
use App\Http\Controllers\Backend\ExhibitionController as BackendExhibitionController;
use App\Http\Controllers\Backend\GameController as BackendGameController;
use App\Http\Controllers\Backend\GenreController as BackendGenreController;
use App\Http\Controllers\Backend\PlatformController as BackendPlatformController;
use App\Http\Controllers\Backend\PublisherController as BackendPublisherController;
use App\Http\Controllers\Backend\RSSItemController;
use App\Http\Controllers\Backend\RSSItemController as BackendRSSItemController;
use App\Http\Controllers\Backend\RSSWebsiteController as BackendRSSWebsiteController;
use App\Http\Controllers\Backend\SerieController as BackendSerieController;
use App\Http\Controllers\Backend\UserController as BackendUserController;
use App\Http\Controllers\Backend\PageController as BackendPageController;
use App\Http\Controllers\DeveloperController;
use App\Http\Controllers\ExhibitionController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PlatformController;
use App\Http\Controllers\PublisherController;
use App\Http\Controllers\RSSCrawlerController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SitemapController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'admin'], function() {

    $exceptShow = ['except' => [ 'show' ] ];

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('home');

    Route::resource('/articles', App\Http\Controllers\Backend\ArticleController::class);
    Route::resource('/developers', App\Http\Controllers\Backend\DeveloperController::class, $exceptShow);
    Route::resource('/exhibitions', BackendExhibitionController::class, $exceptShow);
    Route::post('/exhibitions/game/add', [BackendExhibitionController::class, 'save_exhibition_game']);
    Route::post('/exhibitions/game/update', [BackendExhibitionController::class, 'update_exhibition_game']);
    Route::resource('/games', BackendGameController::class, $exceptShow);
    Route::post('/games/ajax', [BackendGameController::class, 'ajax']);
    Route::resource('/genres', BackendGenreController::class, $exceptShow);
    Route::resource('/platforms', BackendPlatformController::class, $exceptShow);
    Route::resource('/publishers', BackendPublisherController::class, $exceptShow);
    Route::resource('/rssitems', BackendRSSItemController::class, $exceptShow);
    Route::resource('/rsswebsites', BackendRSSWebsiteController::class);
    Route::resource('/series', BackendSerieController::class)->parameters(['series' => 'serie']);;
    Route::resource('/users', BackendUserController::class, $exceptShow);
    Route::resource('/pages', BackendPageController::class)->parameters([
        'pages' => 'admin_pages'
    ]);

    Route::get('removeDuplicates', [RSSCrawlerController::class, 'removeDuplicates']);
    Route::get('games/recently-in-rss', [BackendGameController::class, 'recentlyInRSS']);
    Route::get('games/recently-in-rss/coupling', [BackendGameController::class, 'recentlyInRSSCoupling']);
    Route::get('games/find-publisher/{game}', [BackendGameController::class, 'findPublisher']);
    Route::get('games/find-developer/{game}', [BackendGameController::class, 'findDeveloper']);
    Route::get('games/create/{title}', [BackendGameController::class, 'create']);
    Route::get('games/enrich', [BackendGameController::class, 'enrich']);
    Route::get('/news', [BackendArticleController::class, 'index']);
    Route::get('publishers/create/{title}', [BackendPublisherController::class, 'create']);
    Route::get('rssitems/find-keywords', [BackendRSSItemController::class,'findKeywords']);
    Route::get('rssitems/suggest-game-title', [RSSCrawlerController::class, 'suggestGameTitle']);
    Route::get('rssitems/import',  [BackendRSSItemController::class,'import']);
    Route::get('rssitems/link',  [BackendRSSItemController::class,'link']);
    Route::get('rssitems/archive/{dateFrom}/{dateTo?}',  [BackendRSSItemController::class,'archive']);
});

// Sitemap
Route::get('/sitemap.xml', [SitemapController::class, 'index']);
Route::get('/sitemap', [SitemapController::class, 'index']);
Route::get('/sitemap/articles', [SitemapController::class, 'articles']);
Route::get('/sitemap/platforms', [SitemapController::class, 'platforms']);
Route::get('/sitemap/games', [SitemapController::class, 'games']);
Route::get('/sitemap/pages', [SitemapController::class, 'pages']);
Route::get('/rss', [SitemapController::class, 'rss']);

// Articles
Route::get('/news', [ArticleController::class, 'index']);
Route::get('/article/{article}', [ArticleController::class, 'show']);

// Publishers
Route::get('/publishers', [PublisherController::class, 'index']);
Route::get('/publishers/{publisher}', [PublisherController::class, 'show']);

// Developers
Route::get('/developers', [DeveloperController::class, 'index']);
Route::get('/developers/{developer}', [DeveloperController::class, 'show']);

// Platforms
Route::get('/platforms', [PlatformController::class, 'index']);
Route::get('/platforms/{platform}', [PlatformController::class, 'show']);


// Users
Route::get('/users/{user}', [UserController::class, 'show']);
Route::get('/members/{user}', [UserController::class, 'show']);

// Crawler
Route::get('/crawler/crawl', [RSSCrawlerController::class, 'crawl']);

// Games
Route::get('/games/list', [GameController::class, 'listed'])->name('games.list');
Route::get('/games/upcoming', [GameController::class, 'upcoming'])->name('games.upcoming');
Route::resource('/games', GameController::class)->only(['index', 'show'])->name('games', 'games.show');

// Search
Route::post('/search', [SearchController::class, 'index']);
Route::get('/search/{search}', [SearchController::class, 'results']);

// Authentication
Auth::routes();
Route::get('/validate', [ActivateController::class, 'index']);

// Other pages
Route::get('/', [PageController::class, 'home']);

// Very last routes for catching all pages
Route::get('/{exhibition}/lineup', [ExhibitionController::class, 'lineup']);
Route::get('/{exhibition}/exhibitors', [ExhibitionController::class, 'exhibitors']);

Route::get('/{slug}', function($slug) {

    $page = \App\Models\Page::where('slug', '=', $slug)->first();
    if($page != null) {
        $controller = app()->make('\App\Http\Controllers\PageController');
        return $controller->callAction('show', [$page]);
    }
    $exhibition = \App\Models\Exhibition::where('slug', '=', $slug)->first();
    if($exhibition != null) {
        $controller = app()->make('\App\Http\Controllers\ExhibitionController');
        return $controller->callAction('show', [$exhibition]);
    }

    if($exhibition == null && $page == null) {
        abort(404);
    }
});
