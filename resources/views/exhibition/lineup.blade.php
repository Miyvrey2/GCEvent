@extends('layouts.master')

@section('seo')
    @component("components.seo", ["title" => $exhibition->title, "url" => url($exhibition->slug), "description" => $exhibition->excerpt] )
    @endcomponent
@endsection

@section('content')

    <section id="featured_image_section">
        <div class="featured_image yellowpink" style="background-image:url('https://www.gamescomevent.com/img/gamescom_17_010_010.jpg')"></div>
    </section>

    <div class="container show exhibition-show">
        <div class="row">
            <div class="col-md-12">

                {{-- Title --}}
                <h1>Lineup for {{$exhibition->title}}</h1>

                {{--Breadcrumbs--}}
                @component('components.breadcrumbs', ['breadcrumbs' => [$exhibition->slug => $exhibition->title, url($exhibition->slug . '/lineup') => "Lineup for " . $exhibition->title]])
                @endcomponent

            </div>
            <div class="col-md-9">
                @if($exhibition->publishers->count() > 0)
                    @foreach($exhibition->publishers as $publisher)

                        <strong>{{ $publisher->title }}</strong><br>

                        @foreach($publisher->exhibition_game as $game)
                            <a href="{{url('games/' . $game->slug)}}">{{$game->title}}</a><br>
                        @endforeach
                        <br>
                    @endforeach
                @else
                    <p><strong>Get ready for Gamescom 2025, the world’s largest gaming event, returning to Cologne from August 20–24, 2025. While this year’s game lineup is <span style="color: #0076d6;">still unknown</span>, fans can expect an unforgettable experience with cutting-edge gaming technology, industry insights, and exclusive previews. Relive last year’s highlights, including hands-on sessions with blockbuster titles and thrilling e-sports tournaments.</strong></p>
                    <br>
                    <h2 style="padding-bottom: 10px;">Gamescom 2025: The Ultimate Gaming Destination</h2>
                    <p>Mark your calendars for Gamescom 2025, taking place from August 20 to August 24, 2025, at the iconic Koelnmesse in Cologne, Germany. This global gaming extravaganza will once again welcome fans, developers, and industry leaders to celebrate the best of gaming culture.</p><br>
                    <p>Although the official lineup for Gamescom 2025 is yet to be announced, attendees can look forward to exploring cutting-edge gaming technologies, interactive demos, and immersive experiences across the sprawling 230,000 m² of exhibition space. From indie gems to state-of-the-art hardware, Gamescom is the place where the future of gaming comes to life.</p>
                    <br>
                    <h2 style="padding-bottom: 10px;">What to Expect at Gamescom 2025</h2>
                    <p>While specific games have not yet been revealed, Gamescom 2025 promises to deliver:</p>
                    <ul style="padding-top: 10px; padding-left: 20px;">
                        <li><strong>Exclusive Previews</strong>: Be among the first to experience upcoming games and innovations.</li>
                        <li><strong>E-Sports Events</strong>: Cheer on your favorite teams in adrenaline-pumping competitions.</li>
                        <li><strong>Business Opportunities</strong>: Connect with industry professionals in the dedicated Business Area, open to trade visitors and press.</li>
                        <li><strong>Diverse Experiences</strong>: From retro gaming zones to merchandise halls, there’s something for everyone.</li>
                    </ul>
                    <br>
                    <h2 style="padding-bottom: 10px;">Plan Your Visit</h2>
                    Gamescom 2025 kicks off with the highly anticipated Opening Night Live on August 19, 2025, setting the stage for the week ahead. Tickets will be available soon, so stay tuned for updates on pricing and availability.<br>
                    <br>
                    Whether you’re a hardcore gamer, a developer, or just curious about the latest gaming trends, Gamescom 2025 is the must-attend event of the year. Join the community and be part of the world’s largest celebration of gaming culture.<br>
                    <br>
                    For more information, visit the <a href="https://www.gamescom.global/" target="_blank">official Gamescom website</a>.<br>
                @endif
            </div>

            <div class="col-md-3 sidebar">

                @component('exhibition.sidebar', compact('exhibition'))
                @endcomponent

                <div class="horizontal-line"></div>

                <h2>Advertisement</h2>

                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- gamescomevent_com_exhibition_show -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-5113287686102695"
                     data-ad-slot="7139224508"
                     data-ad-format="auto"
                     data-full-width-responsive="true"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>

                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({
                        google_ad_client: "ca-pub-5113287686102695",
                        enable_page_level_ads: true
                    });
                </script>
            </div>
        </div>
    </div>
@endsection