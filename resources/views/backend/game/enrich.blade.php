@extends('backend.layouts.master')

@section('content')

    <style>
        .col-md-offset-4,
        .col-md-4 {
            width: auto;
            padding: 0 15px;
        }
        .col-offset-4 {
            margin-left: 33.33333333%;
        }
        .col-4 {
            width: 33.33333333%;
        }
        .col-md-4 select,
        .col-4 select {
            width: 100%;
        }
        .mb-1 {
            margin-bottom: 1rem;
        }
        @media screen and (min-width: 768px) {
            .col-md-offset-4 {
                margin-left: 33.33333333%;
            }
            .col-md-4 {
                width: 33.33333333%;
            }
        }
    </style>
    <section id="featured_line_section">
        <div class="featured_line greenblue"></div>
    </section>

    <div class="container backend-main">
        <div class="row">
            <div class="col-md-offset-4 col-md-4">
                @if(isset($game))
                    <h2>{{ $game->title }}  &nbsp; <span style="font-size: 16px;">({{ $amountLeft }} left)</span></h2>

                    <div class="mb-1"><small>{!! $googleData !!}</small></div>

                    <form method="POST" action="{{url('/admin/games/' . $game->slug)}}">

                        {{--Set the post method to patch--}}
                        {{ method_field('PATCH') }}

                        {{ csrf_field() }}

                        <div class="form-group">
                            <input type="hidden" class="form-control" id="title" name="title" required  autocomplete="off" value="{{ $game->title }}">
                            <input type="hidden" class="form-control" id="slug" name="slug" required  autocomplete="off" value="{{ $game->slug }}">
                            <input type="hidden" class="form-control" id="backToEnrich" name="backToEnrich" required  autocomplete="off" value="true">
                        </div>

                        <div class="form-group">
                            <label for="developers[]">Developers</label><br>
                            <select class="form-control js-example-basic-multiple form-control" id="developers[]" name="developers[]" multiple="multiple">
                                @if(count($suggestDevelopers) > 0)
                                    @foreach($suggestDevelopers as $id => $developer)
                                        <option value="{{ $id ?? $developer }}" selected>{{ $developer }}</option>
                                    @endforeach
                                @endif

                                @foreach($developers as $id => $developer)
                                    <option value="{{$id}}">{{$developer}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="publishers[]">Publishers</label><br>
                            <select class="form-control js-example-basic-multiple form-control" id="publishers[]" name="publishers[]" multiple="multiple">
                                @if(count($suggestPublishers) > 0)
                                    @foreach($suggestPublishers as $id => $publisher)
                                        <option value="{{$id}}" selected>{{$publisher}}</option>
                                    @endforeach
                                @endif

                                @foreach($publishers as $id => $publisher)
                                    <option value="{{$id}}">{{$publisher}}</option>
                                @endforeach
                            </select>
                        </div>

                        {{--Load the form--}}
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary button-full">Save</button>
                        </div>
                    </form>
                @else
                    <h3>No items left to enrich.</h3>
                @endif
            </div>
        </div>
    </div>


    {{-- jQuery --}}
    <script type="text/javascript" src="//code.jquery.com/jquery-1.12.3.min.js"></script>

    {{--Select 2--}}
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script>

        $(document).ready(function() {
            $('#game_id').select2();
        });

        $(document).ready(function() {
            $('.js-example-basic-multiple').select2({
                tags: true
            });

            tinymce.init({
                selector:'textarea',
                menubar: false,
                plugins: "link",
                statusbar: false
            });
        });
    </script>

@endsection

