<div class="col-md-12">
    @include('backend.components.error')
</div>
<style>
    #ai {
        position: fixed;
        right: 20px;
        bottom: 20px;
        width: 60px;
        height: 60px;
        background: linear-gradient(to bottom right, #4A90E2, #50E3C2);
        border: none;
        border-radius: 50%;
        box-shadow: 0 4px 10px rgba(0, 0, 0, 0.2);
        color: white;
        font-size: 16px;
        font-weight: bold;
        display: flex;
        justify-content: center;
        align-items: center;
        cursor: pointer;
        transition: all 0.3s ease;
        outline: none;
    }

    #ai:hover {
        background: linear-gradient(to top left,  #4A90E2, #50E3C2);
        box-shadow: 0 6px 15px rgba(0, 0, 0, 0.3);
        transform: scale(1.1);
    }

    #ai::after {
        content: "✨";
        font-size: 14px;
        margin-left: 5px;
    }
    .form-group {
        position: relative;
    }
    .tox-tinymce + .suggestion {
        display: block;
        margin: 0 0 20px 0;
        font-size: 10px;
    }
    .select2 + .suggestion,
    input + .suggestion {
        position: absolute;
        top: 0;
        right: 0;
        display: block;
        margin: 6px -1px 0;
        font-size: 10px;
        background: white;
        height: 20px;
        width: 50%;
        overflow: hidden;
        text-align: right;
    }
    .select2 + .suggestion:hover,
    input + .suggestion:hover {
        height: initial;
    }

    .tox-tinymce + .suggestion::before,
    .select2 + .suggestion::before,
    input + .suggestion::before {
        content: 'AI: ';
        background: linear-gradient(to bottom right,rgba(56,142,60,.7),rgba(0,121,107,.7),rgba(0,151,167,.7),rgba(2,136,209,.7));
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
    }
    .tox-tinymce + .suggestion::after,
    .select2 + .suggestion::after,
    input + .suggestion::after {
        content: '';
        position: absolute;
        left: 0;
        bottom: 0;
        width: 100%;
        height: 4px;
        background: linear-gradient(to bottom, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 100%);
    }
    .suggestion:empty {
        display: none;
    }
</style>

<div class="col-md-9">

    {{ csrf_field() }}

    <div class="form-group">
        <label for="title">Title *</label><br>
        <input type="text" class="form-control" id="title" name="title" required autocomplete="off" value="{{$game->title}}">
    </div>

    <div class="form-group">
        <label for="slug">Slug *</label><br>
        <input type="text" class="form-control" id="slug" name="slug" required  autocomplete="off" value="{{$game->slug}}">
    </div>

    <div class="form-group">
        <label for="excerpt">Excerpt</label><br>
        <textarea type="text" class="form-control tinymce-editor" id="excerpt" name="excerpt" autocomplete="off">{{$game->excerpt}}</textarea>
        <span data-suggestion-excerpt="" class="suggestion"></span>
    </div>

    <div class="form-group">
        <label for="body">Body</label><br>
        <textarea type="text" class="form-control tinymce-editor" id="body" name="body" autocomplete="off">{{$game->body}}</textarea>
        <span data-suggestion-body="" class="suggestion"></span>
    </div>

    @if($game->releases != null)
    <div class="row">
        <div class="col-md-3">
            <strong>Released at</strong>
        </div>
        <div class="col-md-3">
            <strong>Console</strong>
        </div>
        <div class="col-md-3">
            <strong>Region</strong>
        </div>
        <div class="col-md-3">
            <strong>Edition</strong>
        </div>
        @foreach($game->releases as $release)
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="form-control" id="releases[{{ $loop->index }}][released_at]" name="releases[{{ $loop->index }}][released_at]" autocomplete="off" value="{{$release->released_at}}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <select class="form-control js-example-basic-multiple form-control platform-to-array" id="releases[{{ $loop->index }}][console][]" name="releases[{{ $loop->index }}][console][]">
                        <option value="">-</option>
                        <option value="{{$release->console}}" selected="selected">{{$platformsReleasedOn->find($release->console)->title}}</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="form-control" id="releases[{{ $loop->index }}][region]" name="releases[{{ $loop->index }}][region]" autocomplete="off" value="{{$release->region}}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="form-control" id="releases[{{ $loop->index }}][edition]" name="releases[{{ $loop->index }}][edition]" autocomplete="off" value="{{$release->edition}}">
                </div>
            </div>
        @endforeach
        @php($latestGameRelease = count($game->releases) ?? 0 + 1)
        <div class="col-md-3">
            <div class="form-group">
                <input type="text" class="form-control" id="releases[{{ $latestGameRelease }}][released_at]" name="releases[{{ $latestGameRelease }}][released_at]" autocomplete="off" value="">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <select class="form-control js-example-basic-multiple form-control platform-to-array" id="releases[{{ $latestGameRelease }}][console][]" name="releases[{{ $latestGameRelease }}][console][]" multiple="multiple">
                    @foreach($platformsReleasedOn as $platform)
                        <option value="{{$platform->id}}">{{$platform->title}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <input type="text" class="form-control" id="releases[{{ $latestGameRelease }}][region]" name="releases[{{ $latestGameRelease }}][region]" autocomplete="off" value="">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <input type="text" class="form-control" id="releases[{{ $latestGameRelease }}][edition]" name="releases[{{ $latestGameRelease }}][edition]" autocomplete="off" value="">
            </div>
        </div>
    </div>
    @endif
</div>

<div class="col-md-3">
    <div class="form-group">
        <label for="publishers[]">Publishers</label><br>
        <select class="form-control js-example-basic-multiple form-control" id="publishers[]" name="publishers[]" multiple="multiple">
            @foreach($game->publishers as $publisher)
                <option value="{{$publisher->id}}" selected>{{$publisher->title}}</option>
            @endforeach

            @foreach($publishers as $publisher)
                <option value="{{$publisher->id}}">{{$publisher->title}}</option>
            @endforeach
        </select>
        <span data-suggestion-publishers="" class="suggestion"></span>
    </div>

    <div class="form-group">
        <label for="developers[]">Developers</label><br>
        <select class="form-control js-example-basic-multiple form-control" id="developers[]" name="developers[]" multiple="multiple">
            @foreach($game->developers as $developer)
                <option value="{{$developer->id}}" selected>{{$developer->title}}</option>
            @endforeach

            @foreach($developers as $developer)
                <option value="{{$developer->id}}">{{$developer->title}}</option>
            @endforeach
        </select>
        <span data-suggestion-developers="" class="suggestion"></span>
    </div>

    <div class="form-group">
        <label for="released_at">Release date</label><br>
        <span class="note"><strong>note:</strong> "2025-00-00" notes as "in 2025"</span>
        <input type="text" class="form-control" id="released_at" name="released_at" autocomplete="off" value="{{$game->released_at}}">
        <span data-suggestion-released_at="" class="suggestion"></span>
    </div>

    <div class="form-group">
        <label for="platforms[]">Platforms</label><br>
        <select class="form-control js-example-basic-multiple form-control" id="platforms[]" name="platforms[]" multiple="multiple">
            @foreach($game->platforms as $platform)
                <option value="{{$platform->id}}" selected>{{$platform->title}}</option>
            @endforeach

            @foreach($platforms as $platform)
                <option value="{{$platform->id}}">{{$platform->title}}</option>
            @endforeach
        </select>
        <span data-suggestion-platforms="" class="suggestion"></span>
    </div>

    <div class="form-group">
        <label for="aliases[]">Aliases</label><br>
        <select class="form-control js-example-basic-multiple form-control" id="aliases[]" name="aliases[]" multiple="multiple">

            @if($game->aliases != null)
                @foreach($game->aliases as $alias)
                    <option value="{{$alias}}" selected>{{$alias}}</option>
                @endforeach
            @endif
        </select>
        <span data-suggestion-aliases="" class="suggestion"></span>
    </div>

    <div class="form-group">
        <label for="series[]">Series</label><br>
        <select class="form-control js-example-basic-multiple form-control" id="series[]" name="series[]" multiple="multiple">

            @if($game->series != null)
                @foreach($game->series as $serie)
                    <option value="{{$serie->id}}" selected>{{$serie->title}}</option>
                @endforeach
            @endif

            @foreach($series as $serie)
                <option value="{{$serie->id}}">{{$serie->title}}</option>
            @endforeach
        </select>
        <span data-suggestion-series="" class="suggestion"></span>
    </div>

    <div class="form-group">
        <label for="genres[]">Genres</label><br>
        <select class="form-control js-example-basic-multiple form-control" id="genres[]" name="genres[]" multiple="multiple">

            @if($game->genres != null)
                @foreach($game->genres as $genre)
                    <option value="{{$genre->id}}" selected>{{$genre->title}}</option>
                @endforeach
            @endif

            @foreach($genres as $genre)
                <option value="{{$genre->id}}">{{$genre->title}}</option>
            @endforeach
        </select>
        <span data-suggestion-genres="" class="suggestion"></span>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary button-full">Save</button>
        <span id="ai" class="btn btn-primary button-full">AI</span>
    </div>

</div>

{{-- jQuery --}}
<script type="text/javascript" src="//code.jquery.com/jquery-1.12.3.min.js"></script>

{{-- jQuery UI --}}
<script type="text/javascript" src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

{{-- Select 2 --}}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script>
    $( function() {

        var games = [
            @foreach($games as $gameItem)
                "{{$gameItem->title}}",
            @endforeach
        ];

        // Help by autocomplete to prevent doubles
        $('#title').autocomplete({
            source: games
        });

        // Set slug
        $( document ).ready( function() {
            var sluggie = slug($('#title').val());

            $('#slug').val(sluggie);

        });

        // Set slug
        $('#title').change( function() {
            var sluggie = slug($('#title').val());

            $('#slug').val(sluggie);

        });
    });

    var slug = function(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;";
        var to   = "AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        return str;
    };

    $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
            tags: true
        });
    });

    $(document).ready(function() {
        $('#ai').on('click', function() {
            $.ajax({
                url: '{{ url('admin/games/ajax') }}', // Vervang met je API-endpoint
                type: 'POST',
                data: { game_id: '{{ $game->id }}', _token: "{{ csrf_token() }}" }, // Je verzoekgegevens
                dataType: 'json',
                success: function(response) {
                    console.log(response);
                    // Loop door het response-object
                    $.each(response, function(key, value) {
                        // Zoek elementen met het juiste data-attribuut en update hun waarde
                        $('[data-suggestion-' + key + ']').attr('data-suggestion-' + key, value);
                        $('[data-suggestion-' + key + ']').text(value);
                    });
                },
                error: function(xhr, status, error) {
                    console.error('AJAX-fout:', status, error);
                }
            });
        });
    });
</script>