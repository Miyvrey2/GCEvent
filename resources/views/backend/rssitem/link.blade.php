@extends('backend.layouts.master')

@section('content')

    <style>
        .col-md-offset-4,
        .col-md-4 {
            width: auto;
            padding: 0 15px;
        }
        .col-offset-4 {
            margin-left: 33.33333333%;
        }
        .col-4 {
            width: 33.33333333%;
        }
        .col-md-4 select,
        .col-4 select {
            width: 100%;
        }
        .mb-1 {
            margin-bottom: 1rem;
        }
        @media screen and (min-width: 768px) {
            .col-md-offset-4 {
                margin-left: 33.33333333%;
            }
            .col-md-4 {
                width: 33.33333333%;
            }
        }
    </style>
    <section id="featured_line_section">
        <div class="featured_line greenblue"></div>
    </section>

    <div class="container backend-main">
        <div class="row">
            <div class="col-md-offset-4 col-md-4">
                @if(isset($rss_item))
                    <h2 class="mb-1">Link RSS item &nbsp; <span style="font-size: 16px;">({{ $part }} / {{ $total }})</span></h2>
                    <h3 class="mb-1">
                        <a href="{{ $rss_item->url }}">
                            {{ $rss_item->title }}
                        </a>
                    </h3>

                    <form method="POST" action="{{url('/admin/rssitems/' . $rss_item->id)}}">

                        {{--Set the post method to patch--}}
                        {{ method_field('PATCH') }}

                        {{ csrf_field() }}

                        <div class="form-group">
                            <input type="hidden" class="form-control" id="title" name="title" required  autocomplete="off" value="{{ $rss_item->title }}">
                            <input type="hidden" class="form-control" id="url" name="url" required  autocomplete="off" value="{{ $rss_item->url }}">
                            <input type="hidden" class="form-control" id="site" name="site" required  autocomplete="off" value="{{ $rss_item->site }}">
                            <input type="hidden" class="form-control" id="verified_at" name="verified_at" required  autocomplete="off" value="{{\Carbon\Carbon::now()}}">
                        </div>

                        <select id="game_id" name="game_id">
                                <option value="">Choose</option>
                            @foreach($games as $id => $game)
                                @if($id == $rss_item->game_id)
                                    <option value="{{$id}}" selected>{{$game}}</option>
                                @else
                                    <option value="{{$id}}">{{$game}}</option>
                                @endif
                            @endforeach
                        </select>


                        <div class="form-group">
                            <label for="game_title">Or Game title *</label><br>
                            <input type="text" class="form-control" id="game_title" name="game_title" autocomplete="off" value="">
                        </div>

                        {{--Load the form--}}
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary button-full">Save</button>
                        </div>
                    </form>
                @else
                    <h3>No items left to link.</h3>
                @endif
            </div>
        </div>
    </div>


    {{-- jQuery --}}
    <script type="text/javascript" src="//code.jquery.com/jquery-1.12.3.min.js"></script>

    {{--Select 2--}}
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script>

        $(document).ready(function() {
            $('#game_id').select2();
        });

    </script>

@endsection

