<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:media="http://search.yahoo.com/mrss/">
    <channel>
        <title>Gamescomevent.com articles</title>
        <link>https://www.gamescomevent.com</link>
        <description>All articles about gaming and more.</description>
        <atom:link href="{{ url()->current() }}" rel="self"/>
        <language>en-EN</language>
        @foreach ($articles as $article)
        <item>
            <guid>{{ url("article/" . $article->slug) }}</guid>
            <title>{{ utf8_encode(html_entity_decode(strip_tags($article->title))) }}</title>
            <link>{{ url("article/" . $article->slug) }}</link>
            <description>{{ utf8_encode(html_entity_decode(strip_tags($article->excerpt))) }}</description>
            <pubDate>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $article->published_at)->toRfc2822String() }}</pubDate>
        </item>
        @endforeach
    </channel>
</rss>