<?php

namespace Tests\Feature\Frontend;

use App\Models\Article;
use App\Models\Game;
use App\Models\Page;
use App\Models\Platform;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SitemapTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_access_sitemap()
    {
        $articles = Article::factory()->count(5)->create();
        $pages = Page::factory()->count(5)->create();
        $platforms = Platform::factory()->count(5)->create();
        $games = Game::factory()->count(5)->create();

        $response = $this->get('/sitemap');
        $response->assertStatus(200);

        $response = $this->get('/sitemap.xml');
        $response->assertStatus(200);
    }

    /** @test */
    public function it_can_access_sitemap_articles()
    {
        $response = $this->get('/sitemap/articles');
        $response->assertStatus(200);
    }

    /** @test */
    public function it_can_access_sitemap_platforms()
    {
        $response = $this->get('/sitemap/platforms');
        $response->assertStatus(200);
    }

    /** @test */
    public function it_can_access_sitemap_games()
    {
        $response = $this->get('/sitemap/games');
        $response->assertStatus(200);
    }

    /** @test */
    public function it_can_access_sitemap_pages()
    {
        $pages = Page::factory()->count(5)->create();
        $response = $this->get('/sitemap/pages');
        $response->assertStatus(200);
    }

    /** @test */
    public function it_can_access_rss()
    {
        $response = $this->get('/rss');
        $response->assertStatus(200);
    }
}
