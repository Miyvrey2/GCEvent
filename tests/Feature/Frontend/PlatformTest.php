<?php

namespace Feature\Frontend;

use App\Models\Platform;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PlatformTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_list_platforms()
    {
        Platform::factory(3)->create();
        $response = $this->get('/platforms');
        $response->assertStatus(200);
    }

    /** @test */
    public function it_can_show_a_platform()
    {
        $platform = Platform::factory()->create();
        $response = $this->get("/platforms/{$platform->slug}");
        $response->assertStatus(200);
    }
}
