<?php

namespace Feature\Frontend;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_show_a_user_via_users_route()
    {
        $user = User::factory()->create();
        $response = $this->get("/users/{$user->id}");
        $response->assertStatus(200);
    }

    /** @test */
    public function it_can_show_a_user_via_members_route()
    {
        $user = User::factory()->create();
        $response = $this->get("/members/{$user->id}");
        $response->assertStatus(200);
    }
}
