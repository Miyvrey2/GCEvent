<?php

namespace Feature\Frontend;

use App\Models\Publisher;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PublisherTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_list_publishers()
    {
        Publisher::factory(3)->create();
        $response = $this->get('/publishers');
        $response->assertStatus(200);
    }

    /** @test */
    public function it_can_show_a_publisher()
    {
        $publisher = Publisher::factory()->create();
        $response = $this->get("/publishers/{$publisher->slug}");
        $response->assertStatus(200);
    }
}
