<?php

namespace Feature\Frontend;

use App\Models\Game;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GameTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test that the /games/list route returns a list of games.
     */
    public function test_games_list_route_returns_games()
    {
        Game::factory()->create(['released_at' => now()->addDays(10)->format('Y-m-d')]);

        $response = $this->get('/games/list');

        $response->assertStatus(200)
                 ->assertViewIs('game.listed') // Assuming the view name is games.list
                 ->assertViewHas('games');

        $games = $response->viewData('games');
        $this->assertCount(1, $games);
    }

    /**
     * Test that the /games/upcoming route returns upcoming games.
     */
    public function test_upcoming_games_route_returns_upcoming_games()
    {
        // Create games with future release dates
        Game::factory()->create(['released_at' => now()->addDays(10)->format('Y-m-d')]);
        Game::factory()->create(['released_at' => now()->addDays(20)->format('Y-m-d')]);

        // Create games with past release dates
        Game::factory()->create(['released_at' => now()->subDays(5)->format('Y-m-d')]);

        $response = $this->get('/games/upcoming');

        $response->assertStatus(200)
                 ->assertViewIs('game.upcoming') // Assuming the view name is games.upcoming
                 ->assertViewHas('games');

        $games = $response->viewData('games');
        $this->assertCount(2, $games);

        foreach ($games as $game) {
            $released_at = \Carbon\Carbon::parse($game->released_at);
            $this->assertTrue($released_at->isFuture());
        }
    }

    /**
     * Test that the /games route returns a paginated list of games.
     */
    public function test_games_index_route_returns_paginated_games()
    {
        Game::factory()->count(15)->create();

        $response = $this->get('/games');

        $response->assertStatus(200)
                 ->assertViewIs('game.index') // Assuming the view name is games.index
                 ->assertViewHas('games');

        $games = $response->viewData('games');
        $this->assertEquals(15, $games->count()); // Assuming default pagination size is 10
    }

    /**
     * Test that the /games/{id} route returns a single game.
     */
    public function test_games_show_route_returns_single_game()
    {
        $game = Game::factory()->create();

        $response = $this->get("/games/{$game->slug}");

        $response->assertStatus(200)
                 ->assertViewIs('game.show') // Assuming the view name is games.show
                 ->assertViewHas('game');

        $returnedGame = $response->viewData('game');
        $this->assertEquals($game->id, $returnedGame->id);
    }

    /**
     * Test that accessing a non-existent game returns a 404 error.
     */
    public function test_non_existent_game_returns_404()
    {
        $response = $this->get('/games/9999');

        $response->assertStatus(404);
    }
}
