<?php

namespace Feature\Frontend;

use App\Models\Developer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DeveloperTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_list_developers()
    {
        Developer::factory(3)->create();
        $response = $this->get('/developers');
        $response->assertStatus(200);
    }

    /** @test */
    public function it_can_show_a_developer()
    {
        $developer = Developer::factory()->create();
        $response = $this->get("/developers/{$developer->slug}");
        $response->assertStatus(200);
    }
}
