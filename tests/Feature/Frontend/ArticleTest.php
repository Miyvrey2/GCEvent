<?php

namespace Feature\Frontend;

use App\Models\Article;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_list_articles()
    {
        Article::factory(3)->create();
        $response = $this->get('/news');
        $response->assertStatus(200);
    }

    /** @test */
    public function it_can_show_an_article()
    {
        $article = Article::factory()->create();
        $response = $this->get("/article/{$article->slug}");
        $response->assertStatus(200);
    }
}
