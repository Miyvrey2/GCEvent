<?php

namespace Tests\Unit;

use App\Models\Developer;
use App\Models\Game;
use App\Models\Publisher;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\QueryException;
use Tests\TestCase;
use Carbon\Carbon;

class GameTest extends TestCase
{

    use RefreshDatabase;

    public function test_game_can_be_created_with_valid_data()
    {
        $data = [
            'title' => 'Elden Ring',
            'slug' => 'elden-ring',
            'excerpt' => 'An open-world action RPG.',
            'body' => 'Elden Ring is an action role-playing game developed by FromSoftware.',
            'released_at' => now(),
        ];

        $game = Game::create($data);

        $this->assertDatabaseHas('games', [
            'title' => 'Elden Ring',
            'slug' => 'elden-ring',
        ]);

        $this->assertEquals($data['title'], $game->title);
        $this->assertEquals($data['slug'], $game->slug);
    }

    public function test_game_requires_a_title_and_slug()
    {
        $this->expectException(QueryException::class);
        Game::factory()->create(['title' => null]);

        $this->expectException(QueryException::class);
        Game::factory()->create(['slug' => null]);
    }

    public function test_game_slug_must_be_unique()
    {
        $data = [
            'title' => 'First Game',
            'slug' => 'unique-slug',
            'excerpt' => 'An excerpt.',
            'body' => 'A body.',
            'released_at' => now(),
        ];

        Game::create($data);

        $this->expectException(QueryException::class);

        Game::create($data); // Attempt to create a game with the same slug
    }

    public function test_game_can_have_a_publisher()
    {
        $publisher = Publisher::factory()->create();
        $game = Game::factory()->hasAttached($publisher)->create();

        $this->assertTrue($game->publishers->contains($publisher));
        $this->assertEquals($game->publishers->count(), 1);
    }

    public function test_game_can_have_multiple_publishers()
    {
        $publishers = Publisher::factory()->count(3)->create();
        $game = Game::factory()->hasAttached($publishers)->create();

        $this->assertEquals($game->publishers->count(), 3);
    }

    public function test_game_can_have_a_developer()
    {
        $developer = Developer::factory()->create();
        $game = Game::factory()->hasAttached($developer)->create();

        $this->assertTrue($game->developers->contains($developer));
        $this->assertEquals($game->developers->count(), 1);
    }

    public function test_game_can_have_multiple_developers()
    {
        $developers = Developer::factory()->count(3)->create();
        $game = Game::factory()->hasAttached($developers)->create();

        $this->assertEquals($game->developers->count(), 3);
    }
}
